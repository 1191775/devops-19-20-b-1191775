CA4 Tutorial
============

Author: Manuel Pedro Santos Conceição - 1191775

-------------------------------------------------------------------------------------

**The goal of this assignment is to use Docker to setup a containerized environment to
execute your version of the gradle version of spring basic tutorial application.**

-------------------------------------------------------------------------------------

Docker
------

## How to install docker?

Install Docker. For Windows users:

- Install Docker Desktop if the PC has Windows Pro or Enterprise.
- Install Docker Toolbox if you have Windows Home.

For the pourpose of this tutorial, Docker Toolbox was used.

After installing Docker Toolbox  you may encouter a problem with the following message:

`Looks like something went wrong in step 'Setting env'`

This means Docker terminal could not run. It is necessary to comment the following line in the script start.sh located in docker's installation directory and save:

```
##eval "$("${DOCKER_MACHINE}" env --shell=bash --no-proxy "${VM}" | sed -e "s/export/SETX/g" | sed -e "s/=/ /g")" &> /dev/null #for persistent Environment Variables, available in next sessions
```

The solution source [here](https://github.com/docker/toolbox/issues/793).

To start, download the docker example teacher provided by the teacher [here](https://bitbucket.org/atb/docker-compose-spring-tut-demo/) into **ca4** folder.

### How to build and execute Docker?

In the folder with the docker-compose.yml file do the build and then execute.

To build:

`docker-compose build`

To execute:

`docker-compose up`

Building and executing for the first time may take a while. 

To view the spring web application, input in the browser following url:

`http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT`

To open the application with the h2 console use the following link:

`http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/H2-console`

When the H2 consoles opens replace the JDBC URL with the following URL:

`jdbc:h2:tcp://192.168.33.11:9092/./jpadb`


### How to use Docker for your project?

To use Docker for your project you need to edit the web dockerfile in order to reflect your project. 
The alterations done using ca2part2 are the following:

- Altered the repository clone to my own: `https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git`
- Altered the WORKDIR to a copy of ca2part2 made for this assignment called ca2part2-v-ca4.
- Gave permission for gradlew.

This is the web dockerfile with the alterations made for this assignment:

```
FROM tomcat

RUN apt-get update -y

RUN apt-get install -f

RUN apt-get install git -y

RUN apt-get install nodejs -y

RUN apt-get install npm -y

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git

WORKDIR /tmp/build/devops-19-20-b-1191775/ca2part2-v-ca4/demo

RUN chmod +x gradlew

RUN ./gradlew clean build

RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

EXPOSE 8080
```

### How to publish an image to [docker hub](https://hub.docker.com)?

- Sign up to docker hub website https://hub.docker.com .
- Create a new docker repository
- Login into Docker hub from the command line:

`docker login`

- Check the ID of the image to publish:

`docker images`

- Tag each image to publish:

`docker tag <IMAGEID> 1191775/ca4-1191775:<TAG>`

I used the following commands to tag my images for ca4:

`docker tag 2d02ab04fea6 1191775/ca4-1191775:web`

`docker tag a4397b7d8acd 1191775/ca4-1191775:db`

- Push each images to the docker repository:

`docker push 1191775/ca4-1191775:<TAG>`

I used the following command to push my images for ca4:

`docker push 1191775/ca4-1191775:web`

`docker push 1191775/ca4-1191775:db`

These images are in the following repository:

`1191775/ca4-1191775` with the tags `web` and `db`.

### How to use a volume to get a copy of the database files?

To copy the database files `jpadb.mv.db` from the folder **app** to the folder **data** it 
was necessary to copy my entire repository with the ca4 folder to a folder that is inside **users**. This was necessary 
because Docker's virtual machine was looking for a shared folder inside the users folder but my repository for DevOps 
is located inside the path `C:\DEV\`. After doing this I was able to copy the doing the commands below:

Access db container command line:
`docker-compose exec db bash`

Copy database from the folder app to folder data:
`cp /usr/src/app/jpadb.mv.db /usr/src/data`

The database should now be inside the folder data of teh project.


### Commit and tag ca4 assignment

Commit and tag ca4 assignment using the following git commands:

```
git commit -a -m "fix #34 Assignment and readme.md for ca4"
git push
git tag ca4
git push origin ca4
```

Kubernetes - The alternative/complement to Docker
----------

Due to time constraints I was not able to implement any solution using Kubernetes.
For future reference I will place kubernetes documentation website for consult.
 
https://kubernetes.io/docs/home/

-------------------------------------------------------------------------------------

# **End of assignement ca4!** #
 
----------------------------------------------------------------------------------- 
 
### Helpful commands and tips (just for future reference) 

ip 192.168.99.100

Push a new image: 
```
docker tag local-image:tagname new-repo:tagname
docker push new-repo:tagname
```

To make a push
`docker push 1191775/ca4-1191775:tagname`


docker login: 1191775

docker repo name: ca4-1191775

-------------------

- Mensagem prof 

Boa tarde.

Quem está a usar o docker toolbox deve fazer 'docker-machine ip default' e ver qual o IP apresentado. Esse é o IP da VM que executa o docker. Depois, nos url, onde aparece localhost deve substituir por esse ip.

------------------------------------------------------------------------------------------


- Purging All Unused or Dangling Images, Containers, Volumes, and Networks

Docker provides a single command that will clean up any resources — images, containers, volumes, and networks — that are dangling (not associated with a container):

`docker system prune`

To additionally remove any stopped containers and all unused images (not just dangling images), add the -a flag to the command:

`docker system prune -a`

More commands:

`docker-compose exec db bash` //abre acesso a linha de comandos do container db      
`cp /usr/src/app/jpadb.mv.db /usr/src/data` // copiar o ficheiro jpadb.mv.db para a pasta data

https://docs.docker.com/storage/volumes/

----------------------------------------------------------------






