Switch - DevOps
===============

CA5 Assignement - Part2 
=======================

Author: Manuel Conceição - 1191775

------------------------------------------------------------------------------------


Pipelines with Jenkins
-----------------------

The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the
tutorial spring boot application, gradle "basic" version (developed in CA2, Part2)


### How to create a new pipeline with Jenkins to build the tutorial a spring boot application?

First, start Jenkins with using the command `java -jar jenkins.war` on windows cmd
inside the directory where the **jenkins.war** file is located. Jenkins should now be running. 
Access Jenkins at http://localhost:8080 .

You should also run Docker VM now since it will be necessary to do the assignement.

Create a new pipeline in Jenkins using the following groovy script. The project used for this will be a gradle project, 
that I have done for a previous assignement **ca2 - part2**.
 


```
pipeline {
     agent any
     
     stages {
         stage('Checkout') {
             steps {
                 echo 'Checking out...'
                 git credentialsId: '1191775-bitbucket', url: 'https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git'
             }
         }
         
         stage('Assemble') {
             steps {
                 echo 'Building...'
                 bat 'cd ca2part2-v-ca4/demo & gradlew clean assemble'
             }
         }
         
         stage ('Test'){
             steps {
              echo 'Testing...'
              bat 'cd ca2part2-v-ca4/demo & gradlew test'
              junit 'ca2part2-v-ca4/demo/build/test-results/test/TEST-com.greglturnquist.payroll.EmployeeTest.xml'
             }
         }

        stage ('Javadoc'){
            steps {
            echo 'Publishing...'
            bat 'cd ca2part2-v-ca4/demo/ & gradlew javadoc'

                publishHTML([
                            reportName: 'Javadoc',
                            reportDir: 'ca2part2-v-ca4/demo/build/docs/javadoc/',
                            reportFiles: 'index.html',
                            keepAll: false,
                            alwaysLinkToLastBuild: false,
                            allowMissing: false
                            ])
            }
        }
 
         stage('Archive') {
             steps {
                 echo 'Archiving...'
                 archiveArtifacts 'ca2part2-v-ca4/demo/build/libs/*'
             }
         }

           stage ('Docker Image') {
                  steps {
                    script {
                      docker.withRegistry('https://index.docker.io/v1/', 'mpscca5part2'){
                      def customImage = docker.build("1191775/ca4-1191775:${env.BUILD_ID}", "ca5/part2")
                      customImage.push()
                    }
                  }
                }
            }

     }
 }
```

I will not explain the **checkout**, **assemble**, **test** and **archive** since they have already been explained in ca5 - part1.

I will next describe the new stages introduced for ca5 - part2.


### How to generate a javadoc?


**Javadoc stage:**

```
stage ('Javadoc'){
    steps {
    echo 'Publishing...'
    bat 'cd ca2part2-v-ca4/demo/ & gradlew javadoc'

        publishHTML([
                    reportName: 'Javadoc',
                    reportDir: 'ca2part2-v-ca4/demo/build/docs/javadoc/',
                    reportFiles: 'index.html',
                    keepAll: false,
                    alwaysLinkToLastBuild: false,
                    allowMissing: false
                    ])
    }
}
```        

This stage generates the javadoc of the project and publish it in Jenkins in HTML format.

In this step I input path to the project `cd ca2part2-v-ca4/demo/` followed by the command `gradlew javadoc`
to produce the javadoc report.

Next, I the filled the *publishHTML* fields to generate the report in HTML format that can be 
seen in the script (reportName, reportDir, reportFiles, etc..). 

For this step it was necessary to install the *publishHTML* plugin that is not included 
in the recomended plugins that appera by default with Jenkins.


### How to generate and publish a docker image?


**Publish image stage:**

```
stage ('Docker Image') {
                  steps {
                    script {
                      docker.withRegistry('https://index.docker.io/v1/', 'mpscca5part2'){
                      def customImage = docker.build("1191775/ca4-1191775:${env.BUILD_ID}", "ca5/part2")
                      customImage.push()
                    }
                  }
                }
            }
```

This stage generates a docker image with Tomcat and the war file and publish it
in the Docker Hub.

I will explain the script:


**Docker registry**

To give access permission to my docker hub container
it is necessary  to enter the registry and the credentials ID. Here is the argument:

 `docker.withRegistry('[registry]', '[credentialsID]')`
 
To obtain the registry, run the command `docker info` in docker terminal. 
Input the path present after **registry** and place it in the first parameter of *docker.withRegistry*.
In my case, it´s something like this:

Registry: `https://index.docker.io/v1/`

Next, we need to create some credentials in Jenkins. 
Go to Jenkins main page and then select *Credentials* on the left menu. 
Click *System* and then *Global credentials (unrestricted)*.
Then *Add Credentials*.

Now it is necessary to create the credentials by giving your 
**docker hub username**,  
**docker hub password**, 
create an **ID** that will be used as a credential 
and fill in a **description**.

Save the created credential at the end. 

Now you can use the **credentials** to fill in the *docker.withRegistry* second parameter.
In my case, it´s something like this:

Credential: `mpscca5part2`

This is the argument I used:

`docker.withRegistry('https://index.docker.io/v1/', 'mpscca5part2')`


**Create and push docker image**

To create a docker image it is necessary to use the following argument:

`def customImage = docker.build("[dockerhub username]/[dockerhub container ID]:${env.BUILD_ID}", "[repository path for dockerfile & Jenkinsfile]")`

In the **first parameter** it is necessary to input *docker hub username* / *docker hub container ID*. 
This is  how I script my parameter:

`1191775/ca4-1191775`

Next it is necessary to add and commit a **jenkinsfile** and a **dockerfile** to the assignement repository (in my case, a bitbucket repository).
These files should be in the same folder. 
After the files are commited, add the path containing the files to the **second parameter** of *docker.build*. 
Here is the path I used:

`ca5/part2`

Now the complete argument used for *docker.build* that I used in my script:

`def customImage = docker.build("1191775/ca4-1191775:${env.BUILD_ID}", "ca5/part2")`

After the image is created it is necessary to push the image. To push the image simply do the following:

`[docker image].push()`

Here is the argument I used for my docker image named **customImage**:

`customImage.push()`
        
        
---------------


**Side note about docker image stage:**

I was not able to build the jenkins script with this last stage. 
My Docker Hub credentials could not be validate, despite being, apparently, correct. 
Me and my colleagues of G4 group - to whom I thank for the time and effort spent - tried to troubleshoot the problem. 
However we were unable to solve the problem. Here is the message error given on Jenkins console output:

```
Using the existing docker config file.Removing blacklisted property: auths$ docker login -u 1191775 -p ******** https://index.docker.io/v1/
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
error during connect: Post http://%2F%2F.%2Fpipe%2Fdocker_engine/v1.40/auth: open //./pipe/docker_engine: The system cannot find the file specified. In the default daemon configuration on Windows, the docker client must be run elevated to connect. This error may also indicate that the docker daemon is not running.

...
...
...

ERROR: docker login failed
Finished: FAILURE
```

My docker VM was running during the whole process.


### Alternative to Jenkins?

I was going to use Buddy as an alternative but due to time restrictions I was not able to.

### Commit and push

    git commit -a -m "fix #36 End of assignment ca5-part2 and readmefile"
    git push
    git tag -a ca5-part2 -m "ca5-part2"
    git push origin ca5-part2

------------------------------------------------------------------------------------------------------
**End of assignment for ca5-part2**
