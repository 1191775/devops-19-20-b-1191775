Switch - DevOps
===============

CA5 Assignement - Part1 
=======================

Author: Manuel Conceição - 1191775

------------------------------------------------------------------------------------


Pipelines with Jenkins
-----------------------

### How to install Jenkins?

Download and run the WAR file version of Jenkins:

1. Download the latest stable Jenkins WAR file to an appropriate directory on your machine at:

    https://www.jenkins.io/doc/book/installing/#war-file

2. Open up a terminal/command prompt window to the download directory.

3. Run the command `java -jar jenkins.war`.

4. Browse to http://localhost:8080 and wait until the Unlock Jenkins page appears.

To unlock Jenkins go see the administrator inside the file  called *initialAdminPassword* located at:

    C:\Users\[user]\.jenkins\secrets\initialAdminPassword
    
Copy the password inside this file into the field to unlock Jenkins.

Next, install the sugested plugins and create the admin user. 
Choose a username and password you will use to access Jenkins.
Fill in the rest of the information required. Save and finish the instance configuration. 
Access Jenkins again at http://localhost:8080/ .  


### How to start using Jenkins? 

First, create a new project: 

1. Start by setting up the credential:

    1.1 Go to global and create new credentials.

    1.2 Choose a username and password.

    1.3 Set an ID for the project and fill in the description.

Next, create a pipeline:

1. Choose *new item*.

2. Choose *pipeline*.

3. Enter the name for the pipeline.

Next, at job script area, enter the Groovy script for the pipeline. Here is my example for the ca5-part1 assignemnt:

 ```
 pipeline {
     agent any
     stages {
         stage('Checkout') {
             steps {
                 echo 'Checking out...'
                 git credentialsId: '1191775-bitbucket', url: 'https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git'
             }
         }
         stage('Assemble') {
             steps {
                 echo 'Building...'
                 bat 'cd ca2/ca2part1/gradle_basic_demo & gradlew clean assemble'
             }
         }
         stage ('Test'){
             steps {
              echo 'Testing...'
              bat 'cd ca2/ca2part1/gradle_basic_demo & gradlew test'
              junit 'ca2/ca2part1/gradle_basic_demo/build/test-results/test/*.xml'
             }
         }
 
 
 
         stage('Archive') {
             steps {
                 echo 'Archiving...'
                 archiveArtifacts 'ca2/ca2part1/gradle_basic_demo/build/distributions/*'
             }
         }
 
 
 
     }
 }
 ```

This script is composed by **3 stages**:

**Checkout stage:**

```
stage('Checkout') {
             steps {
                 echo 'Checking out...'
                 git credentialsId: '1191775-bitbucket', url: 'https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git'
             }
         }

```

This stage checkout the code from the repository.

In this stage I input my repository path and the credentials.

Credentials: 

    1191775-bitbucket

Path to repository:
    
    https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git

**Assemble stage:**

```
stage('Assemble') {
            steps {
                echo 'Building...'
                bat 'cd ca2/ca2part1/gradle_basic_demo & gradlew clean assemble'
            }
        }
```

This stage compiles and produces the archive files with the application.

For the assemble stage I input the path for the **gradle.build** file in the project 
and the `gradlew clean assemble` command.

**Test stage:**

```
stage ('Test'){
            steps {
             echo 'Testing...'
             bat 'cd ca2/ca2part1/gradle_basic_demo & gradlew test'
             junit 'ca2/ca2part1/gradle_basic_demo/build/test-results/test/*.xml'
            }
        }
```

This stage executes the unit tests and publish in Jenkins the Test results.

For the test stage I input path for the unit tests and the path to publish them in the Jenkins Test Results.

Path for unit tests and `gradlew test` command:

`bat 'cd ca2/ca2part1/gradle_basic_demo & gradlew test`

Path to publish the results:

`junit 'ca2/ca2part1/gradle_basic_demo/build/test-results/test/*.xml`


**Archive stage**

```
stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/ca2part1/gradle_basic_demo/build/distributions/*'
            }
        }
```

This stage archives in Jenkins the archive files in the path:

`archiveArtifacts 'ca2/ca2part1/gradle_basic_demo/build/distributions/*`


Save and build the script.

*Note*: I encountered some problems building the pipeline. 
Jenkins could not recognize the `cmd` command. 
I assume this had to do with the `bat` command used in the groovy script. 
I was able to fix the problem by adding the path for the command line (C:\Windows\System32) 
into the path variables of windows. Jenkins did not seem to be able to invoke Windows command line because of this. 

Problem solved.


### Jenkinsfile - How to run a Jenkinsfile from the Repository?

To use a *jenkinsfile* I was able to download an exemple and adapt it with the text editor to suit my needs. 
This script is the result of that adaptation: 

```
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '1191775-bitbucket', url: 'https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git'
            }
        }

        stage('Assemble') {
            steps {
                echo 'Building...'
                bat 'cd ca2/ca2part1/gradle_basic_demo & gradlew clean assemble'
            }
        }

        stage ('Test'){
            steps {
             echo 'Testing...'
             bat 'cd ca2/ca2part1/gradle_basic_demo & gradlew test'
             junit 'ca2/ca2part1/gradle_basic_demo/build/test-results/test/*.xml'
            }
        }

        stage('Archive') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/ca2part1/gradle_basic_demo/build/distributions/*'
            }
        }


    }
}
```

I then commited this *jenkinsfile* to my repository for it to be available to use with Jenkins.

To use this *jenkinsfile* to create a new pipeline:

1. Go to Jenkins and select *new item* to create a new pipeline. 
Choose a name for the pipeline.

2. In the job script area change the definition from *Pipeline script* to *Pipeline script from SCM*.

3. Select the *SCM* to *GIT* and add the repository URL. My repository link used:

    https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git
    
4. Select your credentials for the repository with the username and password.

5. In *Script Path* input the path of the script inside your project. In my case:

    ca5/part1/Jenkinsfile

6. Click *Save*. Then go to the pipeline a do *Build Now*.

If the build is successful, you should see the pipeline built and running in Jenkins.

### Commit and push ca5 - part1

    git commit -a -m "fix #35 End of assignment ca5-part1 and readmefile"
    git push
    git tag -a ca5-part1 -m "CA5-PART1"
    git push origin ca5-part1

------------------------------------------------------------------------------------------------------
**End of assignment for ca5-part1**