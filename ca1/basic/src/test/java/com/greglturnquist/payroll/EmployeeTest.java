package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void employeeContructorHappyCaseTest (){
        Employee john = new Employee(firstName, lastName, description, jobTitle, email);
        assertTrue(john instanceof Employee);
    }

    @Test
    void employeeEnsureExceptionNullEmailTest() {
        //ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            Employee JohnDoe = new Employee("john", "doe", "a person", "unemployed", null);
        });
    }




}