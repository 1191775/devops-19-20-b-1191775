# Class Assignment Week 2

## 1. Analysis, Design and Implementation
### 1.1. You should have a master branch that you use to "publish" the "stable" versions of the Tutorial React.js and Spring Data REST Application.

### 1.2. You should have tags in this branch to mark the versions of the application. You should use a pattern like: major.minor.revision (e.g., 1.2.0).
####1.2.1. Tag the initial version as v1.2.0.**
To create a new tag, the following command was entered:
        
    git tag -a v1.2.0

After the tag is created, it was necessary to explicitly push the tag, since the tag is not pushed with the commit:

    git push origin v1.2.0

 

### 1.3. You should develop new features in branches named after the feature. For instance, a branch named "email-field" to add a new email field to the application.
#### 1.3.1. You should create a branch called email-field.**
To create a new branch in git the following command was entered: 

    git branch email-field

To change to the newly created branch (change the HEAD), input the following command:

    git checkout email-field

Changes were committed with the command:

    git push --set-upstream origin email-field

#### 1.3.2. You should add support for email field.**

To add support for the email field in the application it was necessary to:
1. Added a new string attribute called email and a new argument to the constructor of the file employee.java.
2. Added new email field to the DatabaseLoader class
3. Added a new field to the app.js 

#### 1.3.3. You should also add unit tests for testing the creation of Employees and the validation of its attributes (for instance, no null/empty values).**

Added folder test to the project. 
Created tests.


#### 1.3.4. You should debug the server and client parts of the solution.**

I did not accomplish this step possible due to the problems related with the project, mainly the pom file (I currently have no knowledge to fix the problem). 
When copying the basic folder and opening it in IntelliJ, the project did not run with `mvnw spring-boot:run`. 
To save time and frustration - and due to the main point 
of the assignment being VCS - I focused on learning to work with git instead of fixing the app.

#### 1.3.5. When the new feature is completed (and tested) the code should be merged with the master and a new tag should be created (e.g, v1.3.0).**

To merge the current email-branch with the master branch I changed to the master branch with the command `git checkout master` followed by the command:

    git merge email-field

Now the email-field branch was merged to master.

Added a new tag to mark the newly added email-field feature to the master branch

    git tag -a v1.3.0
    git push origin v1.3.0


### 1.4. You should also create branches for fixing bugs (e.g., "fix-invalid-email").
Created several issues to deal with bug fixes:

    git branch  fix-pom
    git branch  fix-directory-issues
    git branch  fix-maven-issues
    git branch fix-java-util-Objects


#### 1.4.1. Create a branch called fix-invalid-email. The server should only accept Employees with a valid email (e.g., an email must have the "@" sign).**
Created new branch called fix-invalid-email:

    git branch fix-invalid-email

#### 1.4.2. You should debug the server and client parts of the solution.**
#### 1.4.3. When the fix is completed (and tested) the code should be merged into master and a new tag should be created (with a change in the minor number, e.g., v1.3.0 -> v1.3.1)**

Merged fix-invalid-email with the master branch:

    git merge fix-invalid-email

### 1.5. At the end of the assignment mark your repository with the tag ca1.
Created tag to mark the end of the assignment:

    git tag ca1
    git push origin ca1

## 2. Analysis of an Alternative

The choosen alternative is Mercurial. Mercurial is a Version Control System that is very similar to git.

| Git command	|   Hg command	|   Notes	|
|---	|---	|---	|
|   git pull	|   hg fetch / hg pull -u	|   The fetch command is more similar but requires the FetchExtension to be enabled.    |
|   git fetch	|   hg pull	|       |
|   git push	|   hg push -r .	|    By default, git only pushes the current branch.   |
|      git checkout <commit>         |          hg update -c <cset>         |        git checks and reloads (accidentally) removed files       |
|         git checkout [<rev>] -- <file(s)>      |         hg revert [-r <rev>] <file(s)>          |               |
|       git reset --hard        |         hg revert -a --no-backup          |               |
|       git reset --hard HEAD~1        |           hg strip -r .        |               |
|         git revert <commit>      |          hg backout <cset>         |               |
|       git add <new_file>        |          hg add <new_file>         |         Only equivalent when <new_file> is not tracked.      |
|        git add <file>/ git reset HEAD <file>       |          —         |       Not necessary in Mercurial (see shelve below for partial commit support).        |
|         git merge      |            hg merge       |      git merge is capable of octopus merges, while mercurial merge prefers multiple merges         |
|          git status     |         hg outgoing / hg status         |               |
|        git remote add -f remotename url       |         —          |               |
|        git branch -a       |         hg branches          |               |

*source:* https://www.mercurial-scm.org/wiki/GitConcepts

## 3. Implementation of the Alternative

An alternative was not implemented.

## Conclusion

This assignment was was obviously difficult due to some of the problems with maven and the folder structures that I was 
not able to solve. I had several errors with the pom file everytime I tried to copy the *basic* folder of the project. The command `mvnw spring-boot:run`. 
Ultimately I tried to copy the folder basic within IntelliJ.  That gave even more problems. I then decided to focus just on the VCS git part of the assignment.
I was only able to run on the clone from the template project - with the job title feature added in week1 - to run `mvnw spring-boot:run` command and render the page with localhost.
For that reason, the application is not working as intended (or even working for that matter).

However, being the main objective of this assignment to learn how to use git version control, I can say that I have 
learned plenty about how a version control system works and what its purpose. 
I am now able to use git with the command line interface, something I had never done before. I am also able to add a project to version control (with git), 
understand how branches work, how to create tags to mark versions and also pull, push and commit using the command line, 
therefore I am pleased with my progress. Within the next weeks I will try to figure out why I was not successful implementing everything correctly.

## Links & Bibliography

https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic/ (Basic project to use in this assignment)



