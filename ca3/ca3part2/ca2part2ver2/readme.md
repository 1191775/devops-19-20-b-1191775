
Class Assignment 2 - Part 2 Report
==================================

Here you should write the report for the assignment.

You should use Markdown Syntax so that the report is properly rendered in Bitbucket. See how to write README files for Bitbucket in [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html)

The source code for this assignment is located in the folder [ca1/tut-basic](https://bitbucket.org/atb/devops-19-20-rep-template/src/master/ca1/tut-basic)

As a suggestion, the report should have the following sections.

Analysis, Design and Implementation
-----------------------------------

### The goal of Part 2 of this assignment is to convert the basic version (i.e., "basic" folder) of the Tutorial application to Gradle (instead of Maven)

#### 1. In your repository create a new branch called tut-basic-gradle. You should use this branch for this part of the assignment (do not forget to checkout this branch).

Create a new branch "tut-basic-gradle": 
```
git branch tut-basic-gradle
```

Checkout from the current master branch to tut-basic-gradle branch:
```
git checkout tut-basic-gradle
```

Push the branch upstream:
```
git push --set-upstream origin tut-basic-gradle
```

#### 2. As instructed in the readme file of the tutorial, use https://start.spring.io to start a new gradle spring project with the following dependencies: Rest Repositories; Thymeleaf; JPA; H2.

Use https://start.spring.io to generate a new gradle spring project:

1. Select Gradle project.

2. Add the following dependencies:
    - Rest Repositories
    - Thymeleaf
    - JPA
    - H2
    
    Leave all the other options by default.
    
3. Click the generate button to generate and download the Spring zip file.

#### 3. Extract the generated zip file inside the folder "CA2/Part2/" of your repository. We now have an "empty" spring application that can be built using gradle. You can check the available gradle tasks by executing ./gradlew tasks.

Extract the content of the zip file inside the folder of the project (CA2/Part2/).
To check if the Spring application is working, execute the available gradle tasks with the following code:
```
gradlew tasks
```
When the build is completed, the message "BUILD SUCCESSFUL" should appear.

#### 4. Delete the src folder. We want to use the code from the basic tutorial...
Delete the folder src in CA2/Part2/.

#### 5. Copy the src folder (and all its subfolders) from the basic folder of the tutorial into this new folder. Copy also the files webpack.config.js and package.json Delete the folder src/main/resources/static/built/ since this folder should be generated from the javascrit by the tool webpack.
1. Copy the src folder from the basic folder.
2. Copy the webpack.config.js and the package.json.
3. Delete the src/main/resources/static/built/ folder.

#### 6. You can now experiment with the application by using ./gradlew bootRun. Notice that the web page http://localhost:8080 is empty! This is because gradle is missing the plugin for dealing with the frontend code!
To try the application enter the code:
```
gradlew bootRun
```
Try to display the application with [localhost](http://localhost:8080). The application is not yet running because Gradle is missing a plugin for dealing with the frontend code.

#### 7. We will add the gradle plugin org.siouan.frontend to the project so that gradle is also able to manage the frontend. See https://github.com/Siouan/frontend-gradle-plugin This plugin is similar to the one used in the Maven Project (frontend-maven-plugin). See https://github.com/eirslett/frontend-maven-plugin
Add the plugin org.siouan.frontend so the gradle project is able to display frontend:

- https://github.com/Siouan/frontend-gradle-plugin

#### 8. Add the following line to the plugins block in build.gradle:
```
id "org.siouan.frontend" version "1.4.1"
```
Add the previous line to plugin block of code of the project file build.gradle.

#### 9. Add also the following code in build.gradle to configure the previous plugin:
```
frontend {
nodeVersion = "12.13.1"
assembleScript = "run webpack"
}
```

Add the previous block of code to the build.gradle file.

#### 10. Update the scripts section/object in package.json to configure the execution of webpack (second line inside "scripts"):
```
"scripts": {
"watch": "webpack --watch -d",
"webpack": "webpack"
},
```

Add the previous block of code to configure the execution of webpack in the package.json file.

#### 11. You can now execute ./gradlew build. The tasks related to the frontend are also executed and the frontend code is generated.
Execute gradle to generate the front end:
```
gradlew build
```

Before doing this step it may be necessary to update nodejs and to perform a `gradlew clean`.
A message BUILD SUCCESSFUL should appear.

#### 12. You may now execute the application by using ./gradlew bootRun
Execute the application using the code:
```
gradlew bootRun
```
Localhost now show the application running.

#### 13. Add a task to gradle to copy the generated jar to a folder named "dist" located a the project root folder level
Create a task to copy the generated jar to a folder named dist with the following block of code:
```
task copyJarFile(type: Copy) {
    from 'build/libs/springboot-0.0.1-SNAPSHOT.jar' into 'dist'
}
```

To run the created task, enter the command:
```
gradlew copyJarFile
```

#### 14. Add a task to gradle to delete all the files generated by webpack (usually located at src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean.
To delete all files generated by webpack use the following block of code:
```
clean.doFirst {
       delete 'src/main/resources/static/built/.'
       }
```
This code will run everytime a `gradlew clean` is performed. 

#### 15. Experiment all the developed features and, when you feel confident that they all work commit your code and merge with the master branch.
Perform a `gradlew bootrun` to verify if the [localhost](http://localhost:8080/) is showing the expected result.

#### 16. Document all your work in a readme.md file (including the analysis if alternatives, if developed).
All the work is documented in this readme.md file in a tutorial style.


Analysis of an Alternative
--------------------------

At the end of part 2 of this assignment mark your repository with the tag ca2-part2.
For this assignment you should present an alternative technological solution for the
build automation tool (i.e., not based on Gradle or Maven).
You should Analyze how the alternative solution compares to your base solution. You
should:

#### 1. Present how the alternative tool compares to Gradle regarding build automation features;

#### 2. Describe how the alternative tool could be used (i.e., only the design of the solution) to solve the same goals as presented for this assignment (see previous slides); To potentially achieve a complete solution of the assignment you should also implement the alternative design presented in the previous item 2.

Implementation of the Alternative
---------------------------------

