
Class Assignment 3 - Part 2 Report
==================================
Author: **Manuel Conceição - 1191775**



Analysis, Design and Implementation
-----------------------------------

### Download initial solution
 
 Download the solution provided [here](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/) and paste the content into your ca3part2 folder.
 This folder contains a solution provided of a project using vagrant.

Perform the command `vagrant up` in the host command line on the directory where the vagrantfile is. This will create a new virtual box with the configurations setted in the vagrant file (vagrant must be installed on the host machine).

If everything goes as planned, there should be two virtual machines created:

- part2_db_
- part2_web_

To verify if the VMs are running correctly, use the following links provided:
```
http://localhost:8080/basic-0.0.1-SNAPSHOT/
http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/
http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console
```

### Use vagrant on your own project

Make a copy of your own project to a new folder. I placed my project inside the folder ca3part2, with the name ca2part2ver2.

To apply vagrant to your project, copy the vagrant file previously used or [download it](https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/) into your project folder. Make the necessary changes so it can work with your project.

The following changes were made to the vagrantfile placed in ca2part2ver2:

```
# Change the following command to clone your own repository!
      git clone https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git
      cd ca3\ca3part2\ca2part2ver2
```

After these changes to the vagrantfile, run the command `vagrant up` in the command line, inside the folder with the vagrant file.

After these changes to the vagrant file, run the command `vagrant reload --provision` to provision the VMs.




### End of ca3part2


        git commit -a -m "fix #32 readme.md ca3part2"
        git push
        git tag ca3-part2
        git push origin ca3-part2













