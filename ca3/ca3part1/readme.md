
Class Assignment 3 - Part 1 Report
==================================
Author: **Manuel Conceição - 1191775**


Login virtual machine DevOpsCA3: **manuelconceicao**

Analysis, Design and Implementation
-----------------------------------

The goal of the Part 1 of this assignment is to practice with VirtualBox using the same projects from the previous assignments but now inside a VirtualBox VM with Ubuntu

To create a virtual machine using Oracle Virtual Manager, follow the steps:

- Create a new VM (virtual machine).

-  Change the settings of the created virtual machine:

-  Download ISO of [Ubuntu 18.04 minimalCD](https://help.ubuntu.com/community/Installation/MinimalCD).    

-  Select the ISO in the settings of the VM: 

- Go to storage and select Controller : IDE and select the downloaded Ubuntu ISO. 

-  Configure the VM for 2024 RAM.

-  Set Network Adapter 1 as Nat

-  Set Network Adapter 2 as Host-only Adapter (vboxnet0)

- Start the VM and install Ubuntu.

- Install the dependencies of the projects (e.g., git, jdk, maven, gradle, etc.)

install network tools with command 

```sudo apt install net-tools```

- Set up the IP in network configuration file with the command:

```sudo nano /etc/netplan/01-netcfg.yaml```

Change the IP  of the second adapter to 192.168.56.100:

```
network:
version: 2
renderer: networkd
ethernets:
enp0s3:
dhcp4: yes
enp0s8:
addresses:
- 192.168.56.100/24
```

Apply the changes made to the file with the command:

```sudo netplan apply```

- Install openssh with command:

```sudo apt install openssh-server```

- Install ftp protocol transfer files with command:

```sudo apt install vsftpd```

Enable write access for ftp by doing:

```sudo nano /etc/vsftpd.conf```

Then uncomment the line `write_enable=YES` followed by `sudo service vsftpd restart` to restart ftp service.

- Install gradle with command:

```sudo apt install gradle```

- Install git with command:

```sudo apt install git```

- Install openjdk-8-jdk-headless with command:

```sudo apt install openjdk-8-jdk-headless```

- Install maven with command:

```sudo apt install maven```

----------------------------------------------------------------------------------
**Optional**: You can operate the virtual machine from your host computer using openssh.

To install openssh in the virtual machine, enter the following command: `sudo apt install openssh-server`.

You can now access your vm from the host command line. 
e.g. `ssh manuelconceicao@192.168.56.100` followed by the password.
    
------------------------------------------------------------------------------------

To clone your repository, go to bitbucket and copy the link with the git clone and run the command inside the vm.

e.g.  ```git clone https://1191775@bitbucket.org/1191775/devops-19-20-b-1191775.git```

The clone should now be available.


- Change the directory to the basic folder of the cloned project:
 
```cd devops-19-20-b-1191775/ca1/basic ```

Try to build the project with `gradle build`. It is possible that gradle build may not execute in the VM because it does not have a Desktop.

e.g. **indicar o erro que deu ao correr gradle build**



To give permission for maven, type the command: 

```chmod +x mvnw```

Then run the project with the command:

`./mvnw spring-boot:run`


To access the project application in the browser:

`http://192.168.56.100:8080/`


To commit the readme file to ca3part1:

`git commit -a -m "fix # 33 Add readme.md ca3part1"`
`git push`
`git tag ca3part1`
`git push origin ca3part1`



