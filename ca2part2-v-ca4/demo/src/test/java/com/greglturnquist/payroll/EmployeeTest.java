package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EmployeeTest {

    @Test
    void EmployeeConstructorHappyCase() {
        Employee employee = new Employee("Antonio", "Silva", "Student", "ajms@gmail.com");
        assertTrue(employee instanceof Employee);
    }

    @Test
    void setFirstNameEnsureExceptionWithNull() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(null, "Silva", "Student", "ajms@gmail.com");
        });
    }

    @Test
    void setFirstNameEnsureExceptionWithEmpty() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("", "Silva", "Student", "ajms@gmail.com");
        });
    }

    @Test
    void setFirstNameEnsureExceptionWithSpaces() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(" ", "Silva", "Student", "ajms@gmail.com");
        });
    }

    @Test
    void setLastNameEnsureExceptionWithNull() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", null, "Student", "ajms@gmail.com");
        });
    }

    @Test
    void setLastNameEnsureExceptionWithEmpty() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "", "Student", "ajms@gmail.com");
        });
    }

    @Test
    void setLastNameEnsureExceptionWithSpaces() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", " ", "Student", "ajms@gmail.com");
        });
    }

    @Test
    void setDescriptionEnsureExceptionWithNull() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "Silva", null, "ajms@gmail.com");
        });
    }

    @Test
    void setDescriptionEnsureExceptionWithEmpty() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "Silva", "", "ajms@gmail.com");
        });
    }

    @Test
    void setDescriptionEnsureExceptionWithSpaces() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "Silva", " ", "ajms@gmail.com");
        });
    }

    @Test
    void setEmailEnsureExceptionWithNull() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "Silva", "Student", null);
        });
    }

    @Test
    void setEmailEnsureExceptionWithEmpty() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "Silva", "Student", "");
        });
    }

    @Test
    void setEmailEnsureExceptionWithSpaces() {
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "Silva", "Student", " ");
        });
    }

    @Test
    void setEmailEnsureExceptionWithoutAt(){
        // ARRANGE
        // ACT
        // ASSERT
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Antonio", "Silva", "Student", "ajms.com");
        });
    }

}
